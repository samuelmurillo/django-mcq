from django.db import models


class Question(models.Model):
    """
        - Improve question-choices matches
        - Add questing genres
        - Rename title
    """
    title = models.CharField(max_length=120, default="")
    choices = models.ManyToManyField('Choice')

    def __str__(self):
        return self.title


class Choice(models.Model):
    """
        - How to allow only one correct=True?
    """
    letter = models.CharField(max_length=10, default='a')
    text = models.CharField(max_length=120, default="")
    correct = models.BooleanField(default=False)

    def __str__(self):
        return self.text
