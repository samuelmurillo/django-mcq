from django import forms

from mcq.models import Choice


class ChoicesForm(forms.Form):
    choices = forms.ModelChoiceField(queryset=Choice.objects.all(),
                                     widget=forms.RadioSelect(attrs={'class': ''}),
                                     initial={'choices': Choice.objects.first()})

    def __init__(self, *args, **kwargs):
        question = kwargs.pop('question', None)
        super(ChoicesForm, self).__init__(*args, **kwargs)

        if question:
            self.fields['choices'].queryset = Choice.objects.filter(question=question)
            self.fields['choices'].widget = forms.RadioSelect(attrs={'class': ''})
            self.fields['choices'].initial = {'choices': str(Choice.objects.filter(question=question).first().id)}
