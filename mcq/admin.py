from django.contrib import admin
from mcq.models import Question, Choice

admin.site.register(Question)
admin.site.register(Choice)
