from django.shortcuts import render, redirect
from mcq.models import Question
from mcq.forms import ChoicesForm


def index(request):
    if request.POST:
        form = ChoicesForm(request.POST)
        if form.is_valid():
            if form.cleaned_data['choices'].correct is True:
                print("correct!")
            else:
                print("incorrect :(")

            return redirect('/')
    else:
        question = Question.objects.first()
        form = ChoicesForm(question=question)

    context = {
        'form': form,
        'question': question,
    }
    return render(request, 'mcq/index.html', context)
